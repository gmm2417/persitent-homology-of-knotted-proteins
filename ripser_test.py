#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 17:01:54 2021

@author: moryoussef
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 13:14:38 2021

@author: gabri
"""

import requests
import csv
import gudhi
import matplotlib.pyplot as plt
import numpy as np
from numpy import loadtxt
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.manifold import MDS
from ripser import ripser
from persim import plot_diagrams
import time

start = time.time()

pers_mat = []
df = pd.read_csv("prot_list.csv")

#Create a filter to only select proteins from two different clusters
filt = (df["Sequence_similarity_cluster"]=="2") | (df["Sequence_similarity_cluster"]=="10")
df_filt = df.loc[filt,"Name"]
df_filt.reset_index(drop=True, inplace=True)
pdb_id = []
    
for prot in df_filt:
    pdb_id.append(prot[0:4])
    
shape = len(pdb_id)

#Initialisze the distance matrix
distance_matrix = np.zeros((shape,shape))

#Function to add interpolation points between any two C_alpha atoms"""
def interpolate(l,p):
    
    new_list = np.zeros((len(l) + p*(len(l)-1),3))
    for k in range(len(l)-1):
        for i in range(p):
            new_list[(p+1)*k+i+1] = l[k] + (((i+1)/p)-1/1000) * (l[k+1]-l[k])
        new_list[(p+1)*k] = l[k]
    
    return new_list


points_to_add = 3
    
for count, prot_id in enumerate(pdb_id):
    url = "https://knotprot.cent.uw.edu.pl/chains/{}/{}/chain.xyz.txt".format(prot_id,df_filt[count][5:len(df_filt[count])+1])
    r = requests.get(url, allow_redirects=True)
    open("prot_{}.txt".format(prot_id), 'wb').write(r.content)
    l = loadtxt("prot_{}.txt".format(prot_id))[:,1:]
    new_l = interpolate(l,points_to_add)
    with open("{}.csv".format(prot_id),"w") as f:
        writer = csv.writer(f)
        writer.writerows(new_l)

    
    #Creating the rips filtration and compute the intervals in dimension 1 or 2
    dgms = ripser(new_l,2,15)["dgms"]
    
    #Add the intervals to the matrix of intervals
    pers_mat.append(dgms[2])
    

#Calculation of the distance matrix between persitence diagrams using the 
#Botleneck distance.
for i in range(shape):
    for j in range(shape):
            distance_matrix[i][j] = gudhi.bottleneck_distance(pers_mat[i],pers_mat[j])


#Multidimensional Scaling in 2 Dimensions using the above diatance matrix to get a 2D 
#configuration and plot the data. K-Means algorithm on the later configuration to get 2 
#clusters and see if they correspond to the 2 class of proteins.
mds = MDS(n_components = 2,dissimilarity="precomputed")
mds_coords = mds.fit_transform(distance_matrix)
kmeans = KMeans(n_clusters = 2,random_state = 0).fit(mds_coords) 
y_kmeans = kmeans.predict(mds_coords)         
Cluster1 = plt.scatter(mds_coords[y_kmeans==0,0],mds_coords[y_kmeans==0,1],s=100,c="red")
Cluster2 = plt.scatter(mds_coords[y_kmeans==1,0],mds_coords[y_kmeans==1,1],s=100,c="blue")
plt.legend((Cluster1,Cluster2),("Cluster coeff 10","Cluster coeff 2"))
plt.show()

end = time.time()
print("elapsed time = ", end - start)