#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 21:21:02 2021

@author: moryoussef
"""

import numpy as np
from ripser import ripser
from gudhi.representations import Landscape
import requests
import csv
from numpy import loadtxt
import pandas as pd
#import tensorflow as tf
from tensorflow import keras


length = 10000
training_data = []
training_label = []
test_set = []
test_label = []
"""1: trefoil knot
0: figure_eight knot"""

t = np.linspace(0,2*np.pi,100)
x = np.cos(t) + 2*np.cos(2*t)
y = np.sin(t) - 2*np.sin(2*t)
z = 2*np.sin(3*t)

X = np.sin(t) + t/10
Y = np.sin(t)*np.cos(t)/2
Z = np.sin(2*t)*np.sin(t/2)/4



#testing set

df = pd.read_csv("prot_list.csv")

#Create a filter to only select proteins from two different clusters
filt = (df["Sequence_similarity_cluster"]=="1") |(df["Sequence_similarity_cluster"]=="2") |(df["Sequence_similarity_cluster"]=="3") | (df["Sequence_similarity_cluster"]=="444")
df_filt = df.loc[filt,"Name"]
df_filt.reset_index(drop=True, inplace=True)
pdb_id = []
    
for prot in df_filt:
    pdb_id.append(prot[0:4])
    
shape = len(pdb_id)

#Initialisze the distance matrix
distance_matrix = np.zeros((shape,shape))

#Function to add interpolation points between any two C_alpha atoms"""
def interpolate(l,p):
    
    new_list = np.zeros((len(l) + p*(len(l)-1),3))
    for k in range(len(l)-1):
        for i in range(p):
            new_list[(p+1)*k+i+1] = l[k] + (((i+1)/p)-1/1000) * (l[k+1]-l[k])
        new_list[(p+1)*k] = l[k]
    
    return new_list


points_to_add = 5
    
for count, prot_id in enumerate(pdb_id):
    url = "https://knotprot.cent.uw.edu.pl/chains/{}/{}/chain.xyz.txt".format(prot_id,df_filt[count][5:len(df_filt[count])+1])
    r = requests.get(url, allow_redirects=True)
    open("prot_{}.txt".format(prot_id), 'wb').write(r.content)
    l = loadtxt("prot_{}.txt".format(prot_id))[:,1:]
    new_l = interpolate(l,points_to_add)
    with open("{}.csv".format(prot_id),"w") as f:
        writer = csv.writer(f)
        writer.writerows(new_l)

    
    #Creating the rips filtration and compute the intervals in dimension 1 or 2
    dgms = ripser(new_l,1,20)["dgms"]
    
    #Add the intervals to the matrix of intervals
    test_set.append(Landscape(num_landscapes=2, resolution=100).fit_transform([dgms[1]]))
    

for i in range(length):
    
    l = np.transpose(np.array([x,y,z]))
    l = l + np.random.normal(0,.3,(100,3))
    dgms = ripser(l,1,15)["dgms"]
    training_data.append(Landscape(num_landscapes=2, resolution=100).fit_transform([dgms[1]]))
    training_label.append(1)
    

for i in range(length):
    
    l = np.transpose(np.array([X,Y,Z]))
    l = l + np.random.normal(0,.3,(100,3))
    dgms = ripser(l,1,15)["dgms"]
    training_data.append(Landscape(num_landscapes=2, resolution=100).fit_transform([dgms[1]]))
    training_label.append(0)




training_data = np.array(training_data)
training_label = np.array(training_label)

model = keras.models.Sequential([
        keras.layers.Flatten(input_shape=(1,200)),
        keras.layers.Dense(3,activation="relu"),
        keras.layers.Dense(3,activation="relu"),
        keras.layers.Dense(2,activation="softmax")
        ])
    
model.compile(optimizer="adam",
              loss="sparse_categorical_crossentropy",
              metrics=["accuracy"]
              )
model.fit(training_data, training_label, epochs=5)



for i in range(275):
    test_label.append(1)

for i in range(70):
    test_label.append(0)

test_set, test_label = np.array(test_set), np.array(test_label)

model.evaluate(test_set,test_label)
